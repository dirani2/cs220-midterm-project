//Andrew Hellinger and Darius Irani
//ahellin1, dirani2

#include  "ppm_swap.h"

void swap(Image *im, int col, int row) {
  for (int i = 0; i < col; i++) {
    for (int j = 0; j < row; j++) {
      unsigned char temp = 0;
      temp = im[(i * rows) + j]->r;
      im[i * rows + j]->r = im[i * rows + j]->g;
      im[i * rows + j]->g = im[i * rows + j]->b;
      im[i * rows + j]->b = temp;
}
  
