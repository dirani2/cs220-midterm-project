// ppm_swap.h
// 601.220, Fall 2018
// Darius Irani dirani2, Andrew Hellinger ahellin1


#ifndef PPM_IO_H
#define PPM_IO_H
#include <stdio.h>
#include "ppm_io.h"

/* Pass pointer to image (assumes im != NULL).
 * Swaps color channels per formula on assignment
 */
void swap(Image* im);

#endif
