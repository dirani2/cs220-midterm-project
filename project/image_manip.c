// image_manip.c
// 601.220, Fall 2018
// Darius Irani dirani2, Andrew Hellinger ahellin1
// Performs manipulation operations on images

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "image_manip.h"
#define PI 3.14159265358979323846264338950


//////////////////////////////////////////////////////////////////                                                                     
//                                                              //                                                                     
// SWAP                                                         //                                                                      
//                                                              //                                                                     
//////////////////////////////////////////////////////////////////

/* Assign old value of green to new value of red, old value for blue
 * to new value for green, and old value for red to new value for blue
 */
void pixel_swap(unsigned char *r, unsigned char *g, unsigned char *b) {

  //dereference r and g and store values in temporary variables
  unsigned char r_old = *r;
  unsigned char g_old = *g;

  //use temporary variables and pointer values to perform swap
  *r = g_old;
  *g = *b;
  *b = r_old;

}

/* Pass pointer to image (assumes im != NULL).
 * Swaps color channels per formula on assignment
 */
int swap(Image * im) {

  //iterate over each pixel in im
  for(int i = 0; i < im->cols * im->rows; i++) {
    //perform pixel_swap for each component of each Pixel
    pixel_swap(&((im->data[i]).r), &((im->data[i]).g), &((im->data[i]).b));
  }

  return 0;
}


//////////////////////////////////////////////////////////////////                                                                      
//                                                              //                                                                     
// GRAYSCALE                                                    //                                                                
//                                                              //                                                                      
//////////////////////////////////////////////////////////////////  

/* Pass pointer to pixel components (r, g, b), function calculates 
 * intensity value per formula on assignment, assigns value to each component
 */
void pixel_grayscale(unsigned char *r, unsigned char *g, unsigned char *b) {

  //define intensity value according to assignment instructions
  int intensity = (0.30 * *r) + (0.59 * *g) + (0.11 * *b);

  //dereference each Pixel component pointer and set value to intensity
  *r = intensity;
  *g = intensity;
  *b = intensity;
}

/* Pass pointer to image (assumes im != NULL).
 * Reassign color channels per formula on assignment
 */
int grayscale(Image * im) {

  for(int i = 0; i < im->cols * im->rows; i++) {
      pixel_grayscale(&((im->data[i]).r), &((im->data[i]).g), &((im->data[i]).b));
  }

  return 0;
}


//////////////////////////////////////////////////////////////////                                                                        
//                                                              //                                                                        
// CONTRAST                                                     //                                                                        
//                                                              //                                                                       
//////////////////////////////////////////////////////////////////  

/* Takes a mapped value and applies the saturating math. If the converted double
 * is >255, it floors it at 255. Likewise, if <0, ceilings it to 0.
 */
void pixel_saturation(double *mapped_value) {

  double max = 255;
  double min = 0;
  if(*mapped_value > max) {
    *mapped_value = max;
  }
  else if(*mapped_value < min) {
    *mapped_value = min;
  }
}

/* Takes a pixel comp that evenly spans 0, [0.5, 0.5] and multiplies by a 
 * specified adjustment factor. Then it converts double back to unsigned char 
 * using saturating math.
 */
void pixel_contrast(unsigned char *r, unsigned char *g, unsigned char *b, double adjustment_factor) {

  double mapped_r = (((double) *r / 255) - 0.5) * adjustment_factor;
  double mapped_g = (((double) *g / 255) - 0.5) * adjustment_factor;
  double mapped_b = (((double) *b / 255) - 0.5) * adjustment_factor;
    
  mapped_r = (mapped_r + 0.5) * 255;
  mapped_g = (mapped_g + 0.5) * 255;
  mapped_b = (mapped_b + 0.5) * 255;

  pixel_saturation(&mapped_r);
  pixel_saturation(&mapped_g);
  pixel_saturation(&mapped_b);

  *r = mapped_r;
  *g = mapped_g;
  *b = mapped_b;
}

/* Pass pointer to image (assume im != NULL).
 * Converts pixel values from unsigned chars to doubles in range that evenly
 * spans 0. Multiply by valid user-supplied adjustment factor.
 */
int contrast(Image *im, double adjustment_factor) {

  if(adjustment_factor < 0) {
    fprintf(stderr, "adjustment factor must be positive.");
    return 5;
  }

  for(int i = 0; i < im->cols * im->rows; i++) {
    pixel_contrast(&((im->data[i]).r), &((im->data[i]).g), &((im->data[i]).b), adjustment_factor);
  }

  return 0;
}


//////////////////////////////////////////////////////////////////                                                                        
//                                                              //                                                                        
// ZOOM_IN                                                      //                                                                        
//                                                              //                                                                        
//////////////////////////////////////////////////////////////////  

/* Pass pointer to image (assumes im != NULL).
 * Allocates new memory for larger image, assigns image
 * pointer to new image, frees memory from old image.
 */
int zoom_in(Image * im) {

  //dynamically allocate array to store 4x as many Pixels as im holds
  Pixel *result = malloc(sizeof(Pixel) * 4 * im->rows * im->cols);

  //if allocation failed, print error and return 8
  if (!result) {
    fprintf(stderr, "Error: Could not allocate memory.\n");
    return 8;
  }

  //outer loop iterates over each row
  for (int i = 0; i < im->rows; i++) {

    //store row offset value in variable for future indexing
    int offset = i * 2 * im->cols;

    //inner loop iterates over each column in row
    for (int j = (i * im->cols); j < (i * im->cols + im->cols); j++) {
  
      //store original values of r, g, and b in variables for readability
      unsigned char r = im->data[j].r;
      unsigned char g = im->data[j].g;
      unsigned char b = im->data[j].b;

      //define index for top left pixel in result
      int tl_index = j * 2 + offset;

      //assign im color values to index
      result[tl_index].r = r;
      result[tl_index].g = g;
      result[tl_index].b = b;

      //define index for top right pixel in result
      int tr_index = j * 2 + 1 + offset;

      //assign im color values to index
      result[tr_index].r = r;
      result[tr_index].g = g;
      result[tr_index].b = b;

      //define index for top right pixel in result      
      int bl_index = j * 2 + im->cols * 2 + offset;

      //assign im color values to index      
      result[bl_index].r = r;
      result[bl_index].g = g;
      result[bl_index].b = b;

      //define index for top right pixel in result      
      int br_index = j * 2 + im->cols * 2 + 1 + offset;

      //assign im color values to index
      result[br_index].r = r;
      result[br_index].g = g;
      result[br_index].b = b;
    }
  }

  //free old Pixel array
  free(im->data);

  //double size of im cols and rows to account for larger image
  im->cols = im->cols * 2;
  im->rows = im->rows * 2;

  //make data in im point to new Pixel array
  im->data = result;

  return 0;
}


//////////////////////////////////////////////////////////////////                                                                       
//                                                              //                                                                       
// ZOOM_OUT                                                     //                                                                       
//                                                              //                                                                       
//////////////////////////////////////////////////////////////////

/* Takes the values of 4 pixels of a given channel and averages them together.
 * Sets pixel in new Pixel struct to average of four pixels.
 */
void avg_pixels(unsigned char *a, unsigned char *b, unsigned char *c, unsigned char *d, unsigned char *avg) {
    *avg = (unsigned char) ((*a + *b + *c + *d) / 4.0);
}


/* Takes the values of 4 pixels of a given channel and averages them together.
 * Sets pixel in new Pixel struct to average of four pixels.
 */
void avg_pixels_1D(unsigned char *a, unsigned char *b, unsigned char *avg) {
    *avg = (unsigned char) ((*a + *b) / 2.0);
}

/* Allocates new memory for smaller image, assigns image
 * Handles case when image passed was a 1D column vector.
 * pointer to new image, frees memory from old image.
 */
void zoom_out_column_vector(Image *im, Pixel *result, int *rows_end) {
    int top, bottom;
      int cur = 0;
      for(int i = 0; i < *rows_end; i += 2) {
        top = i;
        bottom = i + 1;
        avg_pixels_1D(&((im->data[top]).r), &((im->data[bottom]).r), &((result[cur]).r)); 
        
        avg_pixels_1D(&((im->data[top]).g), &((im->data[bottom]).g), &((result[cur]).g)); 
              
        avg_pixels_1D(&((im->data[top]).b), &((im->data[bottom]).b), &((result[cur]).b)); 
        cur++;
      }

      im->cols = im->cols;
      im->rows = im->rows / 2;
      
}

/* Allocates new memory for smaller image, assigns image
 * Handles case when image passed was a 1D row vector.
 * pointer to new image, frees memory from old image.
 */
void zoom_out_row_vector(Image *im, Pixel *result, int *cols_end) {
    int left, right;
      int cur = 0;
      for(int i = 0; i < *cols_end; i += 2) {
        left = i;
        right = i + 1;
        avg_pixels_1D(&((im->data[left]).r), &((im->data[right]).r), &((result[cur]).r)); 
        
        avg_pixels_1D(&((im->data[left]).g), &((im->data[right]).g), &((result[cur]).g)); 
              
        avg_pixels_1D(&((im->data[left]).b), &((im->data[right]).b), &((result[cur]).b)); 
        cur++;
      }

      im->cols = im->cols / 2;
      im->rows = im->rows;
}



/* Pass pointer to image (assumes im != NULL).
 * Allocates new memory for smaller image, assigns image
 * pointer to new image, frees memory from old image.
 */
int zoom_out(Image *im) {
    // 1x1 image is at maximum it can be zoomed out, so return
    if(im->rows <= 1 && im->cols <= 1) {
        return 0;
    }

    int cols_end = im->cols;
    int rows_end = im->rows;
    if(im->cols % 2 == 1 && im->cols > 1) {
        cols_end--;
    }
    if(im->rows % 2 == 1 && im->rows > 1) {
        rows_end--;
    } 

    // edge case for 1D column vector
    if(im->cols <= 1 && im->rows > 1) {
        Pixel *result = malloc(sizeof(Pixel) * rows_end * cols_end / 2);
        zoom_out_column_vector(im, result, &rows_end);

        //free old Pixel array
        free(im->data);

        //make data in im point to new Pixel array
        im->data = result;
    }

    // edge case for 1D row vector
    else if(im->rows <= 1 && im->cols > 1) {
        Pixel *result = malloc(sizeof(Pixel) * rows_end * cols_end / 2);
        zoom_out_row_vector(im, result, &cols_end);
        
        //free old Pixel array
        free(im->data);

        //make data in im point to new Pixel array
        im->data = result;
    }

    // normal case, which averages 2x2 submatrix into 1x1 pixel
    else {
        Pixel *result = malloc(sizeof(Pixel) * rows_end * cols_end / 4);
        // for indexing positions in im->data
        int top_left, top_right, bottom_left, bottom_right;
        int cur = 0;
        for(int i = 0; i < rows_end; i += 2) {
            for(int j = i * cols_end; j < i * cols_end + cols_end; j += 2) {
                top_left = j;
                top_right = j + 1;
                bottom_left = j + im->cols;
                bottom_right = j + 1 + im->cols;

                avg_pixels(&((im->data[top_left]).r), &((im->data[top_right]).r),
                    &((im->data[bottom_left]).r), &((im->data[bottom_right]).r), &((result[cur]).r)); 
              
                avg_pixels(&((im->data[top_left]).g), &((im->data[top_right]).g),
                    &((im->data[bottom_left]).g), &((im->data[bottom_right]).g), &((result[cur]).g)); 
              
                avg_pixels(&((im->data[top_left]).b), &((im->data[top_right]).b),
                    &((im->data[bottom_left]).b), &((im->data[bottom_right]).b), &((result[cur]).b));
                cur++;
            }
        }
        //half size of im cols and rows to account for smaller image
        im->cols = im->cols / 2;
        im->rows = im->rows / 2;

        //free old Pixel array
        free(im->data);

        //make data in im point to new Pixel array
        im->data = result;
    }

  return 0;
}


//////////////////////////////////////////////////////////////////                                                                        
//                                                              //                                                                      
// OCCLUDE                                                      //                                                                       
//                                                              //                                                                       
//////////////////////////////////////////////////////////////////

/* Pass pointers to pixel components (r, g, b). Function sets all components
 * to 0, making the pixel black.
 */
void pixel_occlude(unsigned char *r, unsigned char *g, unsigned char *b) {

  //dereference each Pixel component and set each to 0 (black)
  *r = 0;
  *g = 0;
  *b = 0;

}

/* Pass pointer to image (assumes im != NULL).
 * Swaps color channels per formula on assignment
 * Returns an int specifying potential error conditions
 */
int occlude(Image * im, int top_x, int top_y, int bottom_x, int bottom_y) {

  //store cols and rows in variables for readablity
  int cols = im->cols;
  int rows = im->rows;

  //check for errors
  
  //if bottom-right corner pixel row is smaller than the top-left corner pixel's row,
  //or if bottom-right corner pixel column is smaller than the the top-left corner
  //pixel's column, print an error and return 6
  if (top_x > bottom_x || top_y > bottom_y) {
    fprintf(stderr, "Error: Invalid corner indices.\n");
    return 6;
  }

  //if top-left corner index is greater than or equal to rows/cols or is less
  //than 0, print error and return 6
  if (top_x >= cols || top_x < 0 || top_y >= rows || top_y < 0) {
    fprintf(stderr, "Error: Top corner index out of range.\n");
    return 6;
  }

  //if bottom-right corner index is greater than or equal to rows/cols or is less
  //than 0, print error and return 6
  if (bottom_x >= cols || bottom_x < 0 || bottom_y >= rows || bottom_y < 0) {
    fprintf(stderr, "Error: Bottom corner index out of range.\n");
    return 6;
  }

  //if indices are valid, outer loop iterates each row between y indices
  for(int i = top_y; i <= bottom_y; i++) {
    //inner loop iterates over each column between x indices
    for (int j = top_x; j <= bottom_x; j++) {
      //call pixel_occlude to set each pixel to 0 (black)
      pixel_occlude(&((im->data[i * cols + j]).r), &((im->data[i * cols + j]).g), &((im->data[i * cols + j]).b));

    }
  }

  //if this statement reached, no errors, return 0
  return 0;
}


//////////////////////////////////////////////////////////////////                                                                        
//                                                              //                                                                        
// BLUR                                                         //                                                                       
//                                                              //                                                                       
////////////////////////////////////////////////////////////////// 

/* Takes in double, returns its square.
*/
double sq(double sigma) {
    return sigma * sigma;
}

/* Applies formula for gaussian blur at specific index in gaussian filter.
 * Makes use of Math library and defined PI constant.
 */
void generate_gaussian(double *g, double sigma, double dx, double dy) {
    *g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
}

/* Generates a guassian filter based on the size of the filter (determined by)
 * user input. Calculates gaussian blur per formula and populates dynamically 
 * allocated array.
 */
void create_filter(double *filter,int filter_size, double sigma) {
    double dx, dy;
    for(int i = 0; i < filter_size; i++) {
        for(int j = i * filter_size; j < i * filter_size + filter_size; j++) {
            dx = abs(filter_size / 2 - j % filter_size);
            dy = abs(filter_size / 2 - i);
            generate_gaussian(&(filter[j]), sigma, dx, dy);
        }
    }
}

/* Keeps a weighted sum of the rgb values multiplied by the overlayed filter.
 * Each subsequent call to apply_pixel in one iteration of convolve adds to sum.
 * Returns sum as a double.
 */
double apply_pixel(unsigned char *rgb_val, double *filter_val) {
    return (double) *rgb_val * *filter_val;
}

/* Normalizes pixel value by dividing weighted sum by filter sum for all indices in
 * filter utilized.
 */
void normalize_pixel(unsigned char *pixel, double *rgb_weighted_sum, double *filter_sum) {
    *pixel = (unsigned char) (*rgb_weighted_sum / *filter_sum);
}

/* Convolves pixels with filter by calling apply_pixel function for rgb channel values
 * and corresponding filter value. Keeps running sum of the filter values being used.
 * Normalizes weighted sum with filter sum and puts this unsigned char value to corresponing
 * rgb channel in new Pixel struct.
 */
void convolve_pixel(Pixel *pixel, Image *im, double *filter, int filter_size, int pixel_pos) {
    int start = pixel_pos - filter_size / 2 - (filter_size / 2) * im->cols;
    int filter_start;
    int filter_pos = -1;
    double red = 0, green = 0, blue = 0;
    double filter_sum = 0;
    for(int i = 0; i < im->cols * filter_size; i += im->cols) {
        filter_start = pixel_pos - (filter_size / 2) * (1 + filter_size) + (i / im->cols) * filter_size - 1;
        for(int j = i + start; j < i + start + filter_size; j++) {
            filter_start++;
            filter_pos++;

            // this checks the boundary conditions of the data matrix, and determines
            // which elements of the guassian filter to use. Conditions checked:
            // (1) will part of the guassian filter extend past the top row
            // (2) will part of the guassian filter extend past the left column
            // (3) will part of the guassian filter extend past the right column
            // (4) will part of the guassian filter extend past the bottom row
            if(j < 0 || j  >= i + start + im->cols
                    || (j >= 0 && pixel_pos % im->cols < im->cols - filter_size / 2 
                        && j % im->cols > (pixel_pos + filter_size / 2) % im->cols)
                    || (j >= 0 && pixel_pos % im->cols > filter_size / 2 
                        && j % im->cols < (pixel_pos - filter_size / 2) % im->cols)
                    || j >= im->cols * im->rows) {
                continue;
            }
            filter_sum += filter[filter_pos];
            red += apply_pixel(&((im->data[j]).r), &(filter[filter_pos]));
            green += apply_pixel(&((im->data[j]).g), &(filter[filter_pos]));
            blue += apply_pixel(&((im->data[j]).b), &(filter[filter_pos]));
        }
    }
    normalize_pixel(&(pixel->r), &red, &filter_sum);
    normalize_pixel(&(pixel->g), &green, &filter_sum);
    normalize_pixel(&(pixel->b), &blue, &filter_sum);
}

/* Pass pointer to image (assumes im != NULL).
 * Determines appropriate size for gaussian filter by multiplying user inputted
 * sigma by 10. If the product is even, it adds one to ensure it is an odd x odd 
 * filter. Creates guassian filter than for each pixel in im->data, convolves it with
 * gaussian filter, using that pixel as the central pixel in filter.
 */
int blur(Image *im, double sigma) {
    if(!sigma) {
        return 0;
    }

    // specify size for filter
    int filter_size = ceil(10 * sigma);
    if(!(filter_size % 2)) {
        filter_size++;
    }

    // create filter
    double * filter = malloc(filter_size * filter_size * sizeof(double));
    create_filter(filter, filter_size, sigma);

    // apply blur to image
    Pixel * result = calloc(im->rows * im->cols, sizeof(Pixel));
    for(int i = 0; i < im->cols * im->rows; i++) {
        convolve_pixel(&result[i], im, filter, filter_size, i);
    }
    
    free(filter);
    free(im->data);


    //make data in im point to new Pixel array
    im->data = result;

    return 0;
}
