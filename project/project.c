// project.c
// Andrew Hellinger, ahellin1, Darius Irani, dirani2
// 601.220, Fall 2018
// Takes arguments from command line and modifies image accordingly

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ppm_io.h"
#include "image_manip.h"

int main(int argc, char *argv[]) {

  //////////////////////////////////////////////////////////////////
  //                                                              //
  // OPEN AND READ INPUT FILE                                     //
  //                                                              //
  //////////////////////////////////////////////////////////////////
  
  if (argc < 3) {
    fprintf(stderr, "Error: Please specify image input and output files.\n");
    return 1;
  }

  //open test file, create file pointer
  FILE *fr = fopen(argv[1], "rb");

  //check to ensure opening file was successful
  if (!fr) {
    fprintf(stderr, "Error: Could not open image input file.\n");
    return 2;
  }

  //create pointer to Image struct im
  Image *im;

  //read contents of file pointer, set im to point
  //to newly constructed Image struct
  im = read_ppm(fr);

  //close file pointer
  fclose(fr);

  //if im is null after call to read_ppm, print error and return 3
  if (!im) {
    fprintf(stderr, "Error: Specified input file is not a properly-formatted PPM file, or reading input somehow failed.\n");
    //any dynamically allocated memory should have already been free'd in function before it returned NULL
    return 3;
  }


  //////////////////////////////////////////////////////////////////
  //                                                              //
  // PERFORM OPERATIONS                                           //
  //                                                              //
  //////////////////////////////////////////////////////////////////


  if (argc < 4) {
    fprintf(stderr, "Error: Please specify a valid operation.\n");
    return 4;
  }
  
  //declare error and call to track any errors that functions return
  //and to track whether a function is successfully called
  int error = 0;
  int call = 0;
  char *operation = argv[3];
  for(int i = 0; i < strlen(operation); i++) {
    operation[i] = (char) tolower(operation[i]);
  }

  //swap operation
  if (!strcmp(operation, "swap")) {
    error = swap(im);
    call = 1;
  }

  //grayscale operation
  if (!strcmp(operation, "grayscale")) {
    error = grayscale(im);
    call = 1;
  }

  //contrast operation; if there is no fifth argument or if fifth argument is
  //negative or an invalid number, print error and return with error condition
  if (!strcmp(operation, "contrast")) {
    if(argc < 5) {  
      fprintf(stderr, "Error: Incorrect number of arguments for specified operation.\n");
      free(im->data);
      free(im);
      return 5;
    }
    
    double adjustment_factor = 0;
    int check = sscanf(argv[4], "%lf", &adjustment_factor);
    if(check < 1 || adjustment_factor < 0) {
      free(im->data);
      free(im);
      fprintf(stderr, "Error: Incorrect kind of arguments for specified operation.\n");
      return 5;
    }
    
    error = contrast(im, adjustment_factor);
    call = 1;
  }

  //occlude operation; if there are not eight total arguments or if any arguments
  //cannot be converted to integer, print error and return with error condition. 
  if (!strcmp(operation, "occlude")) {
    if (argc < 8) { 
      free(im->data);
      free(im);
      fprintf(stderr, "Error: Incorrect number of arguments for specified operation.\n");
      return 5;
    }

    int top_x = 0;
    int top_y = 0;
    int bottom_x = 0;
    int bottom_y = 0;
    int check1 = sscanf(argv[4], "%d", &top_x);
    int check2 = sscanf(argv[5], "%d", &top_y);
    int check3 = sscanf(argv[6], "%d", &bottom_x);
    int check4 = sscanf(argv[7], "%d", &bottom_y);
    if(check1 < 1 || check2 < 1 || check3 < 1 || check4 < 1 || top_x < 0
      || top_y < 0 || bottom_x < 0 || bottom_y < 0) {
      free(im->data);
      free(im);
      fprintf(stderr, "Error: Incorrect kind of arguments for specified operation.\n");
      return 5;
    }
    
    error = occlude(im, top_x, top_y, bottom_x, bottom_y);
    call = 1;
  }

  //zoom_in function
  if (!strcmp(operation, "zoom_in")) {
    error = zoom_in(im);
    call = 1;
  }

  //zoom_out function
  if (!strcmp(operation, "zoom_out")) {
    error = zoom_out(im);
    call = 1;
  }

  //blur function; if there is no fifth argument or the fifth argument is negative or
  //not a valid number, print error and return error condition
  if(!strcmp(operation, "blur")) {
    if (argc < 5) {
      fprintf(stderr, "Error: Incorrect number of arguments for specified operation.\n");
      free(im->data);
      free(im);
      return 5;
    }

    double sigma = 0;
    int check = sscanf(argv[4], "%lf", &sigma);
    if(check < 1 || sigma < 0) {
      free(im->data);
      free(im);
      fprintf(stderr, "Error: Incorrect kind of arguments for specified operation.\n");
      return 5;
    }
    
    error = blur(im, sigma);
    call = 1;
  }

  //if no function was called, print error and return with error state
  if (!call) {
    fprintf(stderr, "Error: Missing or invalid operation name.\n");
    free(im->data);
    free(im);
    return 4;
  }

  //if the called function reported an error, return the error
  if (error) {
    free(im->data);
    free(im);
    return error;
  }

  //////////////////////////////////////////////////////////////////
  //                                                              //
  // WRITE TO OUTPUT FILE                                         //
  //                                                              //
  ////////////////////////////////////////////////////////////////// 
  
  // write image to disk
  FILE *fw = fopen(argv[2], "wb");
  if (!fw) {
    fprintf(stderr, "Uh oh. Output file failed to open!\n");
    free(im->data);
    free(im);
    return 1;
  }

  int num_pixels_written = write_ppm(fw, im);
  fclose(fw);
  printf("Image created with %d pixels.\n", num_pixels_written);
  
  // clean up!
  free(im->data);
  free(im);

  return 0;

}
