//test_ppm.c
//Andrew Hellinger, ahellin1, Darius Irani, dirani2

#include <stdio.h>
#include <stdlib.h>
#include "ppm_io.h"
#include "image_manip.h"

int main() {

  // allocate space for an Image                                                                                                          
  Image * im = malloc(sizeof(Image));
  if (!im) {
    fprintf(stderr, "Error: Image allocation failed!\n");
    return 1;
  }

  // specify dimensions for our Image                                                                                                     
  im->rows = 1;
  im->cols = 1;


  // allocate space for array of Pixels                                                                                                   
  Pixel *pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  if (!pix) {
    fprintf(stderr, "Error: Pixel array allocation failed!\n");
    free(im);
    return 1;
  }

  // let data field of Image point to the new array                                                                                      
  im->data = pix;

  // define color for Pixel
  Pixel my_color = {0, 127, 255};                                                                              

  //set each Pixel to color
  for (int r = 0; r < im->rows; r++) {
    for (int c = 0; c < im->cols; c++) {
      im->data[(r * im->cols) + c] = my_color;
    }
  }
  
  // write image to disk                                                                                                                            
  FILE *fw = fopen("original.ppm", "wb");
  if (!fw) {
    fprintf(stderr, "Error: Output file failed to open!\n");
    free(im->data);
    free(im);
    return 1;
  }

  write_ppm(fw, im);
  fclose(fw);

  // clean up!                                                                                                                                        
  free(im->data);
  free(im);

  ////////////////////////////////////////////////////////////
  //                                                        //
  // BEGIN FUNCTION TESTING                                 //
  //                                                        //
  ////////////////////////////////////////////////////////////

  //for error handling
  int error = 0;


  /* 
   * test swap() on an image of one pixel
   */

  //open test file, create file pointer                                                                                                   
  FILE *fr_swap = fopen("original.ppm", "rb");

  //test to ensure file opened correctly
  if (!fr_swap) {
    fprintf(stderr, "Error: Could not open file.\n");
    return 2;
  }
  
  //create pointer to Image struct                                                                                                        
  Image *im_swap;

  //read contents of file pointer, im points to contents                                                                                              
  im_swap = read_ppm(fr_swap);

  //close file pointer                                                                                                                               
  fclose(fr_swap);

  //check to ensure read_ppm was successful
  if (!im_swap) {
    fprintf(stderr, "Error: Could not read image.\n");
    return 3;
  }
  
  //perform function
  swap(im_swap);

  // write image to disk                                                                                                                             
  FILE *fw_swap = fopen("swap.ppm", "wb");
  if (!fw_swap) {
    fprintf(stderr, "Error: Output file failed to open!\n");
    free(im_swap->data);
    free(im_swap);
    return 1;
  }

  write_ppm(fw_swap, im_swap);
  fclose(fw_swap);

  // clean up!                                                                                                                                      
  free(im_swap->data);
  free(im_swap);


  /*                                                                                                                                    
   * test grayscale() on an image of one pixel                                                                                            
   */

  //open file
  FILE *fr_grayscale = fopen("original.ppm", "rb");
  if (!fr_grayscale) {
    fprintf(stderr, "Error: Could not open file.\n");
    return 2;
  }
  
  Image *im_grayscale;
  im_grayscale = read_ppm(fr_grayscale);
  fclose(fr_grayscale);

  if (!im_grayscale) {
    fprintf(stderr, "Error: Could not read image.\n");
    return 3;
  }
  
  //perform function                                                                                                                                  
  grayscale(im_grayscale);

  // write image to disk
  FILE *fw_grayscale = fopen("grayscale.ppm", "wb");
  if (!fw_grayscale) {
    fprintf(stderr, "Error: Output file failed to open!\n");
    free(im_grayscale->data);
    free(im_grayscale);
    return 1;
  }
  write_ppm(fw_grayscale, im_grayscale);
  fclose(fw_grayscale);

  // clean up!                                                                                                                            
  free(im_grayscale->data);
  free(im_grayscale);


  /*                                                                                                                                     
   * test contrast() on an image of one pixel                                                                                            
   */

  //open file                                                                                                                            
  FILE *fr_contrast = fopen("original.ppm", "rb");
  if (!fr_contrast) {
    fprintf(stderr, "Error: Could not open file.\n");
    return 2;
  }

  Image *im_contrast;
  im_contrast = read_ppm(fr_contrast);
  fclose(fr_contrast);

  if (!im_contrast) {
    fprintf(stderr, "Error: Could not read image.\n");
    return 3;
  }
    
  //perform function                                                                                                                                  
  error = contrast(im_contrast, 0.5);
  if (error != 0) {
    printf("Error at function contrast.\n");
    return error;
  }

  // write image to disk                                                                                                                              
  FILE *fw_contrast = fopen("contrast0.5.ppm", "wb");
  if (!fw_contrast) {
    fprintf(stderr, "Error: Output file failed to open!\n");
    free(im_contrast->data);
    free(im_contrast);
    return 1;
  }
  write_ppm(fw_contrast, im_contrast);
  fclose(fw_contrast);

  // clean up!                                                                                                                                        
  free(im_contrast->data);
  free(im_contrast);


  /*                                                                                                                                                  
   * test occlude() on an image of one pixel                                                                                                        
   */

  //open file                                                                                                                    
  FILE *fr_occlude = fopen("original.ppm", "rb");
  if (!fr_occlude) {
    fprintf(stderr, "Error: Could not open file.\n");
    return 2;
  }

  Image *im_occlude;
  im_occlude = read_ppm(fr_occlude);
  fclose(fr_occlude);

  if (!im_occlude) {
    fprintf(stderr, "Error: Could not read image.\n");
    return 3;
  }
  
  //perform function                                                                                                                   
  error = occlude(im_occlude, 0, 0, 0, 0);
  if (error != 0) {
    printf("Error at function occlude.\n");
    return error;
  }

  // write image to disk                                                                                                   
  FILE *fw_occlude = fopen("occlude.ppm", "wb");
  if (!fw_occlude) {
    fprintf(stderr, "Error: Output file failed to open!\n");
    free(im_occlude->data);
    free(im_occlude);
    return 1;
  }
  write_ppm(fw_occlude, im_occlude);
  fclose(fw_occlude);

  // clean up!                                                                                                                           
  free(im_occlude->data);
  free(im_occlude);

  /*                                                                                                                                                  
   * test zoom_in() on an image of one pixel                                                                                                          
   */

  //open file                                                                                                                                         
  FILE *fr_zoom_in = fopen("original.ppm", "rb");
  if (!fr_zoom_in) {
    fprintf(stderr, "Error: Could not open file.\n");
    return 2;
  }

  Image *im_zoom_in;
  im_zoom_in = read_ppm(fr_zoom_in);
  fclose(fr_zoom_in);

  if (!im_zoom_in) {
    fprintf(stderr, "Error: Could not read image.\n");
    return 3;
  }
  
  //perform function                                                                                                                                  
  zoom_in(im_zoom_in);

  // write image to disk                                                                                                                              
  FILE *fw_zoom_in = fopen("zoom_in.ppm", "wb");
  if (!fw_zoom_in) {
    fprintf(stderr, "Error: Output file failed to open!\n");
    free(im_zoom_in->data);
    free(im_zoom_in);
    return 1;
  }
  write_ppm(fw_zoom_in, im_zoom_in);
  fclose(fw_zoom_in);

  // clean up!                                                                                                                                        
  free(im_zoom_in->data);
  free(im_zoom_in);


                                                                                                                                     
   /* test zoom_out() on an image of one pixel  */                                                                                             

  //open file                                                                                                                                         
  FILE *fr_zoom_out = fopen("original.ppm", "rb");
  if (!fr_zoom_out) {
    fprintf(stderr, "Error: Could not open file.\n");
    return 2;
  }

  Image *im_zoom_out;
  im_zoom_out = read_ppm(fr_zoom_out);
  fclose(fr_zoom_out);

  if (!im_zoom_out) {
    fprintf(stderr, "Error: Could not read image.\n");
    return 3;
  }
    
  //perform function                                                                                                                                  
  zoom_out(im_zoom_out);

  // write image to disk                                                                                                                              
  FILE *fw_zoom_out = fopen("zoom_out.ppm", "wb");
  if (!fw_zoom_out) {
    fprintf(stderr, "Error: Output file failed to open!\n");
    free(im_zoom_out->data);
    free(im_zoom_out);
    return 1;
  }
  write_ppm(fw_zoom_out, im_zoom_out);
  fclose(fw_zoom_out);

  // clean up!                                                                                                                                        
  free(im_zoom_out->data);
  free(im_zoom_out);

  
  /*                                                                                                                                             
   * test blur() on an image of one pixel                                                                                                      
   */

  //open file                                                                                                                                         
  FILE *fr_blur = fopen("original.ppm", "rb");
  if (!fr_blur) {
    fprintf(stderr, "Error: Could not open file.\n");
    return 2;
  }
  
  Image *im_blur;
  im_blur = read_ppm(fr_blur);
  fclose(fr_blur);

  if (!im_blur) {
    fprintf(stderr, "Error: Could not read image.\n");
    return 3;
  }

  //perform function                                                                                                                                  
  blur(im_blur, 0.5);

  // write image to disk                                                                                                                              
  FILE *fw_blur = fopen("blur.ppm", "wb");
  if (!fw_blur) {
    fprintf(stderr, "Error: Output file failed to open!\n");
    free(im_blur->data);
    free(im_blur);
    return 1;
  }
  write_ppm(fw_blur, im_blur);
  fclose(fw_blur);

  // clean up!                                                                                                                                        
  free(im_blur->data);
  free(im_blur);

  return 0;
  
}
