#ifndef IMAGE_MANIP_H
#define IMAGE_MANIP_H

#include "ppm_io.h"

//////////////////////////////////////////////////////////////////                                                                      
//                                                              //                                                                     
// SWAP                                                   //                                                                
//                                                              //                                                                      
//////////////////////////////////////////////////////////////////  

/* Pass pointer to image (assumes im != NULL).
 * Swaps color channels per formula on assignment
 */
int swap(Image* im);

/* Assign old value of green to new value of red, old value for blue
 * to new value for green, and old value for red to new value for blue
 */
void pixel_swap(unsigned char* r, unsigned char* g, unsigned char* b);


//////////////////////////////////////////////////////////////////                                                                      
//                                                              //                                                                     
// GRAYSCALE                                                    //                                                                
//                                                              //                                                                      
//////////////////////////////////////////////////////////////////  

/* Pass pointer to image (assumes im != NULL).
 * Reassign color channels per formula on assignment
 */
int grayscale(Image* im);

/* Pass pointer to pixel components (r, g, b), function calculates 
 * intensity value per formula on assignment, assigns value to each component
 */
void pixel_grayscale(unsigned char *r, unsigned char *g, unsigned char *b);


//////////////////////////////////////////////////////////////////                                                                      
//                                                              //                                                                     
// CONTRAST                                                    //                                                                
//                                                              //                                                                      
//////////////////////////////////////////////////////////////////  

/* Takes a mapped value and applies the saturating math. If the converted double
 * is >255, it floors it at 255. Likewise, if <0, ceilings it to 0.
 */
void pixel_saturation(double *mapped_value);


/* Takes a pixel comp that evenly spans 0, [0.5, 0.5] and multiplies by a 
 * specified adjustment factor. Then it converts double back to unsigned char 
 * using saturating math.
 */
void pixel_contrast(unsigned char *r, unsigned char *g, unsigned char *b, double adjustment_factor);


/* Pass pointer to image (assume im != NULL).
 * Converts pixel values from unsigned chars to doubles in range that evenly
 * spans 0. Multiply by valid user-supplied adjustment factor.
 */
int contrast(Image *im, double adjustment_factor);


//////////////////////////////////////////////////////////////////                                                                      
//                                                              //                                                                     
// ZOOM_IN                                                    //                                                                
//                                                              //                                                                      
//////////////////////////////////////////////////////////////////  

/* Pass pointer to image (assumes im != NULL).
 * Allocates new memory for larger image, assigns image
 * pointer to new image, frees memory from old image.
 */
int zoom_in(Image* im);

//////////////////////////////////////////////////////////////////                                                                      
//                                                              //                                                                     
// ZOOM_OUT                                                    //                                                                
//                                                              //                                                                      
//////////////////////////////////////////////////////////////////  

/* Takes the values of 4 pixels of a given channel and averages them together.
 * Sets pixel in new Pixel struct to average of four pixels.
 */
void avg_pixels_1D(unsigned char *a, unsigned char *b, unsigned char *avg);


/* Takes the values of 4 pixels of a given channel and averages them together.
 * Sets pixel in new Pixel struct to average of four pixels.
 */
void avg_pixel(unsigned char *a, unsigned char *b, unsigned char *avg);

/* Allocates new memory for smaller image, assigns image
 * Handles case when image passed was a 1D column vector.
 * pointer to new image, frees memory from old image.
 */
void zoom_out_column_vector(Image *im, Pixel *result, int *rows_end);

/* Allocates new memory for smaller image, assigns image
 * Handles case when image passed was a 1D row vector.
 * pointer to new image, frees memory from old image.
 */
void zoom_out_row_vector(Image *im, Pixel *result, int *cols_end);

/* Pass pointer to image (assumes im != NULL).
 * Allocates new memory for smaller image, assigns image
 * pointer to new image, frees memory from old image.
 */
int zoom_out(Image* im);

//////////////////////////////////////////////////////////////////                                                                      
//                                                              //                                                                     
// OCCLUDE                                                    //                                                                
//                                                              //                                                                      
//////////////////////////////////////////////////////////////////  

/* Pass pointer to image (assumes im != NULL).
 * Swaps color channels per formula on assignment
 * Returns an int specifying potential error conditions
 */
int occlude(Image* im, int top_x, int top_y, int bottom_x, int bottom_y);

/* Pass pointers to pixel components (r, g, b). Function sets all components
 * to 0, making the pixel black.
 */
void pixel_occlude(unsigned char *r, unsigned char *g, unsigned char *b);

//////////////////////////////////////////////////////////////////                                                                      
//                                                              //                                                                     
// BLUR                                                    //                                                                
//                                                              //                                                                      
//////////////////////////////////////////////////////////////////  

/* Takes in double, returns its square.
*/
double sq(double sigma);

/* Applies formula for gaussian blur at specific index in gaussian filter.
 * Makes use of Math library and defined PI constant.
 */
void generate_gaussian(double *g, double sigma, double dx, double dy);

/* Generates a guassian filter based on the size of the filter (determined by)
 * user input. Calculates gaussian blur per formula and populates dynamically 
 * allocated array.
 */
void create_filter(double *filter,int filter_size, double sigma);

/* Keeps a weighted sum of the rgb values multiplied by the overlayed filter.
 * Each subsequent call to apply_pixel in one iteration of convolve adds to sum.
 * Returns sum as a double.
 */
double apply_pixel(unsigned char *rgb_val, double *filter_val);

/* Normalizes pixel value by dividing weighted sum by filter sum for all indices in
 * filter utilized.
 */
void normalize_pixel(unsigned char *pixel, double *rgb_weighted_sum, double *filter_sum);

/* Convolves pixels with filter by calling apply_pixel function for rgb channel values
 * and corresponding filter value. Keeps running sum of the filter values being used.
 * Normalizes weighted sum with filter sum and puts this unsigned char value to corresponing
 * rgb channel in new Pixel struct.
 */
void convolve_pixel(Pixel *pixel, Image *im, double *filter, int filter_size, int pixel_pos);

/* Pass pointer to image (assumes im != NULL).
 * Determines appropriate size for gaussian filter by multiplying user inputted
 * sigma by 10. If the product is even, it adds one to ensure it is an odd x odd 
 * filter. Creates guassian filter than for each pixel in im->data, convolves it with
 * gaussian filter, using that pixel as the central pixel in filter.
 */
int blur(Image *im, double sigma);

#endif
