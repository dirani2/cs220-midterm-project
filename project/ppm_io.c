// ppm_io.c
// Andrew Hellinger, ahellin1, Darius Irani, dirani2 
// 601.220, Fall 2018
// Reads and writes images

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"


/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  //////////////////////////////////////////////////////////////////                                                                   
  //                                                              //                                                                     
  // CHECK FILE POINTER, DECLARE VARIABLES                        //                                                                      
  //                                                              //                                                                     
  //////////////////////////////////////////////////////////////////
  
  // check that fp is a valid file pointer
  if(!fp) {
    //if fp is NULL, print error and return NULL
    return NULL;
  }

  //declare variables to store file header information
  char type[3] =  "";
  int max_value = 0;
  char c;

  //////////////////////////////////////////////////////////////////                                                                      
  //                                                              //                                                                     
  // READ HEADER                                                  //
  //                                                              //                                                                     
  ////////////////////////////////////////////////////////////////// 
    
  //read type in header data, store in declared variable
  int check = fscanf(fp, " %s\n", type);

  //if scanf did not succeed, print error and return NULL
  if (check < 1 || strcmp(type, "P6") != 0) {
    return NULL;
  }

  //check for comments; initialize variables to begin while loop
  int found = 0;
  c = getc(fp);

  //while the line begins with a comment symbol
  while (c == '#') {
    //set found to 1, indicates there was a comment line
    found = 1;
    //while newline character is not reached, advance to next char
    while (getc(fp) != '\n') {
       c = getc(fp);
    }
  }

  //if no comments found, put last read char back in stream (first char of cols)
  if (!found) {
    ungetc(c, fp);
  }

  //dynamically allocate memory to store Image
  Image * im = malloc(sizeof(Image));
  if (!im) {
      //if malloc failed, return NULL
      return NULL;
  }
    
  check = fscanf(fp, " %d %d %d ", &(im->cols), &(im->rows), &max_value);

  if (check < 3 || im->cols < 1 || im->rows < 1 || max_value != 255) {
    free(im);
    return NULL;
  }

  //////////////////////////////////////////////////////////////////                                                                      
  //                                                              //                                                                      
  // READ BINARY CONTENTS                                         //                                                                      
  //                                                              //                                                                      
  //////////////////////////////////////////////////////////////////  
    
  //dynamically allocate memory for an array of Pixels
  Pixel *pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  if (!pix) {
    //if malloc fails, free memory for Image, return NULL
    free(im);
    return NULL;
  }
    
  //read pixels from input file, store num read in pix_read
  int pix_read = fread(pix, sizeof(Pixel), im->rows * im->cols, fp);

  //if pix_read is not number of pixels expected based on image dimensions,
  //free memory and return NULL
  if(pix_read != im->rows * im->cols) {
    free(pix);
    free(im);
    return NULL;
  }

  //if no errors, have im->data point to pix, return Image struct pointer im  
  im->data = pix;
  return im;
  
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->rows * im->cols, fp);

  if (num_pixels_written != im->rows * im->cols) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

